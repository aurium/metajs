(function() { // Start test1
// JS equivalent to Macros as Variables

var a = 123;
var b = 432;
var c = 555;

var result = 0;

computeTime('Arithmetic with vars', 5, 2e8, function(repeat) {
  for (var i=0; i<repeat; i++) {
    result +=  a + b;
    result +=  b + c;
    result +=  c + a;
    result += -a - b - c;
    result += -c*1.99;
  }
});

console.log(result);

function sum(a,b){ return parseFloat(a) + parseFloat(b) }

computeTime('Calling functions', 5, 2e7, function(repeat) {
  for (var i=0; i<repeat; i++) {
    result += sum('2', 3);
    result += sum('-3', -2.01);
  }
});

console.log(result);

})(); // End test1
