#!/bin/bash -e

if test -z "$1"; then
  echo "
    Usage:
    \$ $(basename "$0") module-name
    where module-name is the file name without \".metajs\".
  "
  exit 1
fi

function generateJS() {
  file_name="$1"
  parent_sed_script="$2"
  ws="$(echo -en '[ \t]')"
  nws="$(echo -en '[^ \t]')"
  macro_re="^.*//$ws*macro$ws+($nws+)$ws*:$ws*([^\r\n]+).*\$"
  inc_re="^$ws*//$ws*include$ws+($nws+).*\$"

  sed_script="$(
    echo "$parent_sed_script"
    egrep "$macro_re" "$file_name.metajs" |
    while read macro_def; do
      macro_name="$( echo "$macro_def" | sed -r "s#$macro_re#\1#" )"
      macro_expr="$( echo "$macro_def" | sed -r "s#$macro_re#\2#; s#@([1-9])#\\\\\1#g" )"
      if ( echo "$macro_def" | grep -q '@' ); then
        echo "s#$macro_name\(([^),]+),?([^),]+)?\)#$macro_expr#g;"
      else
        echo "s#$macro_name#$macro_expr#g;"
      fi
    done
  )"

  #echo "$sed_script" >&2

  echo "(function() { // Start $file_name"
  sed -r "$sed_script; s#$ws*//$ws*macro$ws.*##g" "$file_name.metajs" |
  while read line; do
    inc_file="$( echo $line | egrep "$inc_re" | sed -r "s#$inc_re#\1#" )"
    test -n "$inc_file" && echo ">> Including $inc_file" >&2
    if test -n "$inc_file"; then
      generateJS "$inc_file" "$sed_script"
    else
      echo "$line"
    fi
  done
  echo "})(); // End $file_name"
}

generateJS "$1"
