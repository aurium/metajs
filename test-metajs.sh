#!/bin/bash -e

if test -z "$1"; then
  echo "
    Usage:
    \$ $(basename "$0") test-name
    where test-name is the file name without \".metajs\".
  "
  exit 1
fi

js_compute_time="
  function computeTime(desc, repeat, internalLoop, fn) {
    var start = Date.now();
    for (var i=0; i<repeat; i++) { console.log('loop '+(i+1)); fn(internalLoop) }
    var end = Date.now();
    var secs = (end-start)/1000;
    console.log(desc+' run '+repeat*internalLoop+' times, in '+secs+' seconds.');
  }
"

js_file="/tmp/$1.js"
compiled_file="compiled-$1.js"
compiled_zip="/tmp/compiled-$1.zip"
js_zip="/tmp/$1.js.zip"

re_clear='s#^ *| *$|//.*##g'
( echo "$js_compute_time"; ./metajs.sh "$1" ) | sed -r "$re_clear" > "$compiled_file"
( echo "$js_compute_time"; cat "$1.js" ) | sed -r "$re_clear" > "$js_file"

test -e "$compiled_zip" && rm "$compiled_zip"
test -e "$js_zip" && rm "$js_zip"
zip "$compiled_zip" "$compiled_file"
zip "$js_zip" "$js_file"

echo "| Files                     | Bytes |"
echo  +---------------------------+-------+
for f in "$1.metajs" "$js_file" "$compiled_file" "$js_zip" "$compiled_zip"; do
  printf "| %-25s | %5d |\n" "$f" $( ls -l "$f" | sed -r "s/.* $USER ([0-9]+) .*/\1/" )
done

