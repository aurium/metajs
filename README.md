metajs — javascript extended
============================

Meta extensions, not a new language.
This is only an experiment for javascript demos, like js1k and js13kGames.

## Features

### Including another file

If you have this files:

`main.metajs`
```javascript
var x = 1;
//include calc
console.log(x);
```

`calc.metajs`
```javascript
var k = 4
x += k;
```

...and you run:
```bash
metajs.sh main > main.js
```

...then you end with:
`main.js`
```javascript
(function() { // Start main
var x = 1;
(function() { // Start calc
var k = 4
x += k;
})(); // End calc
console.log(x);
})(); // End main
```

### Macros

`test.metajs`
```javascript
//macro myConst: 123
//macro sum: ( parseFloat(@1) + parseFloat(@2) )
//macro size: ( @1.length || 0 )
someDiv.style.width = myConst * zoom + "px";
total = sum(someDiv.style.width, otherDiv.style.width) * 3;
for (var i=0; i<size(obj); i++) doSomething(obj[i]);
```

Will render to:
```javascript
someDiv.style.width = 123 * zoom + "px";
total = ( parseFloat(someDiv.style.width) + parseFloat( otherDiv.style.width) ) * 3;
for (var i=0; i<( obj.length || 0 ); i++) doSomething(obj[i]);
```

## Testing

```bash
test-metajs.sh test-name
```

